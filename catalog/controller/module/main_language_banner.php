<?php
class ControllerModuleMainLanguageBanner extends Controller {
	public function index() {


		$this->load->model('module/main_language_banner');

        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick.css');
        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick-theme.css');
        $this->document->addScript('catalog/view/javascript/jquery/slick/slick.js');

        $data['banners'] = array();

        $results = $this->model_module_main_language_banner->getMainBanner();

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {

                $image = $this->model_tool_image->resizeAndConvertImageWebP($result['image'],'-', '-')['image_web_p'];
                $image_old_format = $this->model_tool_image->resizeAndConvertImageWebP($result['image'],'-', '-')['image'];

                $image_mobile = $this->model_tool_image->resizeAndConvertImageWebP($result['image_mobile'],'-', '-')['image_web_p'];
                $image_mobile_old_format = $this->model_tool_image->resizeAndConvertImageWebP($result['image_mobile'],'-', '-')['image'];


                $data['banners'][] = array(
                    'main_banner_id' => $result['main_banner_id'],
                    'title' => $result['title'],
                    'link'  => $result['link'],
                    'image' => $image,
                    'image_mobile' => $image_mobile,
                    'image_old_format' => $image_old_format,
                    'image_mobile_old_format' => $image_mobile_old_format
                );
            }
        }


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/main_language_banner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/main_language_banner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/main_language_banner.tpl', $data);
		}
	}
}