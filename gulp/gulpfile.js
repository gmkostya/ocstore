var config = require( './config' );
// Подключаем Gulp и все необходимые библиотеки
var gulp           = require('gulp'),
    gutil          = require('gulp-util' ),
    sass           = require('gulp-sass'),
    browserSync    = require('browser-sync'),
    cleanCSS       = require('gulp-clean-css'),
    autoprefixer   = require('gulp-autoprefixer'),
    bourbon        = require('node-bourbon'),
    ftp            = require('vinyl-ftp'),
    cssimport = require( 'gulp-cssimport' ),
    postcss = require( 'gulp-postcss' ),
    mqpacker = require( 'css-mqpacker' ),
    gulpIf = require( 'gulp-if' ),
    sourcemaps = require( 'gulp-sourcemaps' )
    // scss = require("gulp-scss"),
    // ocRefresh = require('gulp-opencart-refresh');
//*//
    babel = require( 'gulp-babel' ),    //    npm install @babel/core --save
    eslint = require( 'gulp-eslint' ),
    inject = require( 'gulp-inject-string' ),
    uglify = require( 'gulp-uglify' );
    include = require( 'gulp-include' );
    notify = require( 'gulp-notify' );
    concat = require( 'gulp-concat' );


// Обновление страниц сайта на локальном сервере
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "ocstore", //put your local domain
        notify: false
    });
});


gulp.task('sass', function() {
    var processors = [
        autoprefixer,
        mqpacker()
    ];
    return gulp.src(config.src.mainscss)
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(postcss(processors))
        .pipe(cssimport())
        .pipe(sourcemaps.identityMap())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest.css))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task('js', function () {
    // gulp.src(config.src.js+ '**/*.js')
    gulp.src([config.src.js+ 'jquery-2.1.1.min.js',config.src.js+ 'bootstrap.min.js',config.src.js+ 'common.js'])
        //.pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        // .pipe(uglify()) //Сожмем наш js
        .pipe(concat('build.js', {newLine: '\r\n\r\n\r\n#######################\r\n\r\n\r\n'})) // в какой файл объединить
        .pipe(sourcemaps.write('.')) //Пропишем карты
        .pipe(gulp.dest(config.dest.js)) //Выплюнем готовый файл в build
        .pipe(browserSync.reload({stream: true}));
});


// watch js files and run [js] task after file changed
gulp.task( 'js:watch', function() {
    gulp.watch( config.src.js + '*', [ 'js' ] );
});


// lint js files
gulp.task( 'jslint', function() {
    gulp.src( [ config.src.js + '**/*.js' ] )
        .pipe( eslint() )
        .pipe( eslint.format( 'codeframe' ) );
});


// watch js files and run [jslint] task after file changed
gulp.task( 'jslint:watch', function() {
    gulp.watch( config.src.js + '*', [ 'jslint' ] );
});


// Наблюдение за файлами
gulp.task('watch', ['sass','js', 'browser-sync'], function() {
    gulp.watch('../catalog/view/theme/default/stylesheet/*.scss', ['sass']);
    gulp.watch('../catalog/view/theme/default/template/**/*.tpl', browserSync.reload);
    gulp.watch(config.src.js + '**/*',['js']);
    // gulp.watch('../catalog/view/theme/default/libs/**/*', browserSync.reload);
});


gulp.task('default', ['watch']);

//
// gulp.task('ocRefresh', function(){
//     gulp.src(['./system/**/*.ocmod.xml', './catalog/view/theme/*/template/**/*.tpl'])
//         .pipe(ocRefresh({
//             url: "http://miza/admin",
//             login: "admin",
//             password: "nthb23"
//         }));
// });

// Выгрузка изменений на хостинг
// gulp.task('deploy', function() {
//     var conn = ftp.create({
//         host:      'hostname.com',
//         user:      'username',
//         password:  'userpassword',
//         parallel:  10,
//         log: gutil.log
//     });
//     var globs = [
//         'catalog/view/theme/ghost/**'
//     ];
//     return gulp.src(globs, {buffer: false})
//         .pipe(conn.dest('/path/to/folder/on/server'));
// });

