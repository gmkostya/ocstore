module.exports = {
	src: {
		// root: 'src/',
		// sass: 'src/scss/',
		mainscss: '../catalog/view/theme/default/stylesheet/main.scss',
		mainjs: '../catalog/view/javascript/main.js',
		js: '../catalog/view/javascript/src/',
		// img: 'src/img/',
		// helpers: 'gulp/helpers/'
	},

	dest: {
		// root: 'build/',
		css: '../catalog/view/theme/default/stylesheet/',
		// html: 'build/',
		js: '../catalog/view/javascript/',
		// img: 'build/img/'
	}
};
