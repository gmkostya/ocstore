<?php
class ModelModuleSeoPage extends Model {
	public function addSeoPage($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "seo_page SET link = '" . $this->db->escape($this->normalizeLink($data['link'])) . "' , robots = '" . $this->db->escape($data['robots']) . "' ");
		$link_id = $this->db->getLastId();
		if($data['item']){
			foreach($data['item'] as $language_id => $item){
				$this->db->query("INSERT INTO " . DB_PREFIX . "seo_page_desck SET 
									link_id = '" . $link_id . "', 
									language_id = '" . $language_id . "', 
									meta_title = '" . $this->db->escape($item['meta_title']) . "',
									meta_description = '" . $this->db->escape($item['meta_description']) . "',
									meta_keywords = '" . $this->db->escape($item['meta_keywords']) . "' ");
			}
		}
		return $link_id;
	}

	public function editSeoPage($data){
		$this->db->query("UPDATE " . DB_PREFIX . "seo_page SET link = '" . $this->db->escape($this->normalizeLink($data['link'])) . "', robots = '" . $this->db->escape($this->normalizeLink($data['robots'])) . "' WHERE link_id = '" . (int)$data['link_id'] . "' ");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_page_desck WHERE link_id = '" . (int)$data['link_id'] . "' ");
		if($data['item']){
			foreach($data['item'] as $language_id => $item){
				$this->db->query("INSERT INTO " . DB_PREFIX . "seo_page_desck SET 
									link_id = '" . (int)$data['link_id'] . "', 
									language_id = '" . $language_id . "', 
									meta_title = '" . $this->db->escape($item['meta_title']) . "',
									meta_description = '" . $this->db->escape($item['meta_description']) . "',
									meta_keywords = '" . $this->db->escape($item['meta_keywords']) . "' ");
			}
		}
		return $data['link_id'];
	}

	public function deleteSeoPage($link_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_page WHERE link_id = '" . (int)$link_id . "' ");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_page_desck WHERE link_id = '" . (int)$link_id . "' ");
	}

	public function getSeoPages(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page sp LEFT JOIN " . DB_PREFIX . "seo_page_desck spd ON(sp.link_id=spd.link_id) WHERE spd.language_id = '" . $this->config->get('config_language_id') . "' ");
		$new_array = array();
		if($query->num_rows){
			foreach($query->rows as $item_page){
				$new_array[$item_page['link']] = array(
					'link_id' => $item_page['link_id'],
					'robots' => $item_page['robots'],
					'meta_title' => $item_page['meta_title'],
					'meta_description' => $item_page['meta_description'],
					'meta_keywords' => $item_page['meta_keywords'],
					'edit' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . '&link_id=' . $item_page['link_id'], 'SSL')
				);
			}
		}
		return $new_array;
	}

	public function getOnlySeoPagesLink(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page");
		$new_array = array();
		if($query->num_rows){
			foreach($query->rows as $item_page){
				$new_array[] = array(
					'link_id' => $item_page['link_id'],
					'robots'    => $item_page['robots'],
					'link'    => $item_page['link'],
					'edit' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . '&link_id=' . $item_page['link_id'], 'SSL')
				);
			}
		}
		return $new_array;
	}

	public function getSeoPage($link_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page sp LEFT JOIN " . DB_PREFIX . "seo_page_desck spd ON(sp.link_id=spd.link_id) WHERE sp.link_id = '" . (int)$link_id . "' ");
		$new_array = array();
		if($query->num_rows){
			$new_array['link'] = $query->rows[0]['link'];
			$new_array['link_id'] = $query->rows[0]['link_id'];
			$new_array['robots'] = $query->rows[0]['robots'];

			foreach($query->rows as $item_page){
				$new_array['item'][$item_page['language_id']] = array(
					'link_id' => $item_page['link_id'],
					'meta_title' => $item_page['meta_title'],
					'meta_description' => $item_page['meta_description'],
					'meta_keywords' => $item_page['meta_keywords'],
					'edit' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . '&link_id=' . $item_page['link_id'], 'SSL')
				);
			}
		}

		return $new_array;
	}


	public function normalizeLink($link){
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$last_simbol = substr($link,-1);
		if($last_simbol == '/')
			$link = substr($link, 0, -1);
		$link_now = str_replace(HTTP_CATALOG, '', $link);
		foreach($languages as $item_lang){
			$link_now = str_replace($item_lang['code'] .'/', '', $link_now);
		}
		$link_now = str_replace('ua/', '', $link_now);

		return $link_now;
	}
}
