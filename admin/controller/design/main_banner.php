<?php
class ControllerDesignMainBanner extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('design/banner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/main_banner');

		$this->getForm();
	}

	public function add() {
		$this->load->language('design/banner');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/main_banner');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

//		    echo '<pre>';
//		    var_dump($this->request->post);
//		    echo '</pre>';
//		    exit;
            $this->model_design_main_banner->addMainBanner($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('design/main_banner', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['banner_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_link'] = $this->language->get('entry_link');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_banner_add'] = $this->language->get('button_banner_add');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }


		if (isset($this->error['banner_title'])) {
			$data['error_banner_title'] = $this->error['banner_title'];
		} else {
			$data['error_banner_title'] = array();
		}

		if (isset($this->error['banner_image'])) {
			$data['error_banner_image'] = $this->error['banner_image'];
		} else {
			$data['error_banner_image'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('design/main_banner', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

        $data['action'] = $this->url->link('design/main_banner/add', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['cancel'] = $this->url->link('design/banner', 'token=' . $this->session->data['token'] . $url, 'SSL');

//        $data['current_gallery'] = false;
//        if ($this->request->get['banner_id'] == $this->config->get('gallery_banner_id')) {
//            $data['current_gallery'] = true;
//        }

		if (isset($this->request->get['banner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$banner_info = $this->model_design_banner->getBanner($this->request->get['banner_id']);
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('tool/image');

        $main_banner_images = $this->model_design_main_banner->getMainBannerImages();

        if(isset($this->request->post['banner_image'])){
            $banner_images = $this->request->post['banner_image'];
        }elseif($main_banner_images){
            $banner_images = $main_banner_images;
        }else{
            $banner_images = array();
        }

        $data['banner_images'] = array();

		foreach ($banner_images as $banner_image) {

            $banner_image_description = array();

            foreach ($banner_image['banner_image_description'] as $language_id => $item) {

                $img = array();
                $mob = array();

                if($item['image']){
                    $img['image'] = $item['image'];
                    $img['thumb'] = $this->model_tool_image->resize($item['image'], 100, 100);
                }else{
                    $img['image'] = '';
                    $img['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
                }

                if($item['image_mobile']){
                    $mob['image'] = $item['image_mobile'];
                    $mob['thumb'] = $this->model_tool_image->resize($item['image_mobile'], 100, 100);
                }else{
                    $mob['image'] = '';
                    $mob['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
                }

                $banner_image_description[$language_id] = array(
                    'title' => $item['title'],
                    'image' => $img,
                    'image_mobile' => $mob,
                );
            }

			$data['banner_images'][] = array(
				'banner_image_description' => $banner_image_description,
				'link'                     => $banner_image['link'],
				'sort_order'               => $banner_image['sort_order']
			);
		}


		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');





		$this->response->setOutput($this->load->view('design/main_banner_form.tpl', $data));
	}

	protected function validateForm() {

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));

		if (!$this->user->hasPermission('modify', 'design/banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['banner_image'])) {
			foreach ($this->request->post['banner_image'] as $banner_image_id => $banner_image) {
				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {
					if ((utf8_strlen($banner_image_description['title']) < 2) || (utf8_strlen($banner_image_description['title']) > 64)) {
						$this->error['banner_title'][$banner_image_id][$language_id] = $this->language->get('error_title');
                    }

					if($language_id == $language_info['language_id']){
                        if(empty($banner_image_description['image'])){
                            $this->error['banner_image'][$banner_image_id][$language_id] = 'Необходимо выбрать картинку!';
                        }
                    }
				}
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'design/banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}