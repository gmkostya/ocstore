<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height) {
		if (!is_file(DIR_IMAGE . $filename)) {
			if (is_file(DIR_IMAGE . 'no_image.jpg')) {
				$filename = 'no_image.jpg';
			} elseif (is_file(DIR_IMAGE . 'no_image.png')) {
				$filename = 'no_image.png';
			} else {
				return;
			}
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $old_image);
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $new_image);
			} else {
				copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
			}
		}

		$imagepath_parts = explode('/', $new_image);
		$new_image = implode('/', array_map('rawurlencode', $imagepath_parts));
		
		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $new_image;
		} else {
			return $this->config->get('config_url') . 'image/' . $new_image;
		}
	}


    /**
     * need have extention   Imagick
     * @param $filename  image link frome base
     * @param $width  image width
     * @param $height image height
     * @return array  two variant image ild format and WeBp format
     */
    function resizeAndConvertImageWebP($filename, $width, $height) {


        if (!is_file(DIR_IMAGE . $filename)) {
            if (is_file(DIR_IMAGE . 'no_image.jpg')) {
                $filename = 'no_image.jpg';
            } elseif (is_file(DIR_IMAGE . 'no_image.png')) {
                $filename = 'no_image.png';
            } else {
                return;
            }
        }


        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.'.$extension ;
        $new_imageweb_p = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.webp' ;


        if ((!is_file(DIR_IMAGE . $new_image) || !is_file(DIR_IMAGE . $new_imageweb_p)) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }


            list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

            if (($width_orig != $width AND $width!='-' ) || ($height_orig != $height  AND $height!='-')) {
                $image = new Image(DIR_IMAGE . $old_image);
                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $new_image);
            } elseif($width=='-' AND $height=='-') {

                //original_size
                $image = new Image(DIR_IMAGE . $old_image);
                $image->resize2();
                $image->save(DIR_IMAGE . $new_image, 90);
            } else {

                $image = new Image(DIR_IMAGE . $old_image);
                $image->resize($width_orig, $height_orig);
                $image->save(DIR_IMAGE . $new_image);

            }

            $newWidth = $width;
            $newHeight = $height;

            $imagick = new Imagick(DIR_IMAGE . $new_image);
            $origImageDimens = $imagick->getImageGeometry();
            $origImgWidth = $origImageDimens['width'];
            $origImgHeight = $origImageDimens['height'];

            if ($width=='-') {
                $newWidth = $origImgWidth;
            }
            if ($height=='-') {
                $newHeight = $origImgHeight;
            }


            if ($newWidth == 0 ) {
                $ratioOfHeight = $newHeight / $origImgHeight;
                $newWidth = $origImgWidth * $ratioOfHeight;
            }

            if ($newHeight == 0) {
                $ratioOfWidth = $newWidth / $origImgWidth;
                $newHeight = $origImgHeight * $ratioOfWidth;
            }

            $widthRatios = $origImgWidth / $newWidth;
            $heightRatios = $origImgHeight / $newHeight;

            if ($widthRatios <= $heightRatios) {
                $cropWidth = $origImgWidth;
                $cropHeight = $newWidth * $widthRatios;
            } else {
                $cropWidth = $newHeight * $heightRatios;
                $cropHeight = $origImgHeight;
            }

            $cropX = ($origImgWidth - $cropWidth) / 2;
            $cropY = ($origImgHeight - $cropHeight) / 2;



            $imagick->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $imagick->setImageCompressionQuality(75);
            $imagick->stripImage();
            $imagick->setInterlaceScheme(\Imagick::INTERLACE_PLANE);

            file_put_contents(DIR_IMAGE . $new_image, $imagick->getImageBlob());

            //  $imagick->cropImage($cropWidth, $cropHeight, $cropX, $cropY);
            //  $imagick->resizeImage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 0.9);
            $imagick->setImageFormat('webp');
            $imagick->setImageAlphaChannel(imagick::ALPHACHANNEL_ACTIVATE);
            $imagick->setBackgroundColor(new ImagickPixel('transparent'));
            $imagick->writeImage(DIR_IMAGE . $new_imageweb_p);

        }

        $image_data = array();

        $imagepath_parts = explode('/', $new_imageweb_p);
        $image_web_p = implode('/', array_map('rawurlencode', $imagepath_parts));


        $imagepath_parts = explode('/', $new_image);
        $image = implode('/', array_map('rawurlencode', $imagepath_parts));


        if ($this->request->server['HTTPS']) {
            $image_data['image_web_p'] = $this->config->get('config_ssl') . 'image/' . $image_web_p;
            $image_data['image'] = $this->config->get('config_ssl') . 'image/' . $image;
        } else {
            $image_data['image_web_p'] = $this->config->get('config_url') . 'image/' . $image_web_p;
            $image_data['image'] = $this->config->get('config_url') . 'image/' . $image;
        }

        return $image_data;

    }

}
