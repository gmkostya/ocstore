<?php
// Heading
$_['heading_title']    = 'Галерея';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';
$_['text_banner_empty']        = 'У Вас нет доступных банеров <a href="%s">создать баннер</a>';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_banner']     = 'Укажите баннер в роли галереи';
$_['entry_limit']     = 'Укажите количество фото на странице';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';