<?php
class ModelDesignMainBanner extends Model {
	public function addMainBanner($data) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_banner");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_banner_description");


        if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {

                $this->db->query("INSERT INTO " . DB_PREFIX . "main_banner SET link = '" .  $this->db->escape($banner_image['link']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");

				$main_banner_id = $this->db->getLastId();

				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "main_banner_description SET main_banner_id = '" . (int)$main_banner_id . "', language_id = '" . (int)$language_id . "', title = '" .  $this->db->escape($banner_image_description['title']) . "', image = '" .  $this->db->escape($banner_image_description['image']) . "',  image_mobile = '" .  $this->db->escape($banner_image_description['image_mobile']) . "'");
				}
			}
		}

	}

	public function getMainBannerImages() {
		$banner_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_banner");

        $images = array();

        foreach ($banner_query->rows as $query){

            $banner_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_banner_description WHERE main_banner_id = '". (int)$query['id']."'");

            $banner_image_description = array();

            foreach($banner_image_description_query->rows as $row){
                $banner_image_description[$row['language_id']] = array(
                 'title' => $row['title'],
                 'image' => $row['image'],
                 'image_mobile' => $row['image_mobile'],
                );
            }
            $images[] = array(
              'id' => $query['id'],
              'link' => $query['link'],
              'sort_order' => $query['sort_order'],
              'banner_image_description' => $banner_image_description,
            );
        }

		return $images;
	}

}