<?php
class ControllerProductProductPartial extends Controller {
    public function index( $setting ){

        $products = array();
        foreach ($setting['results'] as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resizeAndConvertImageWebP($result['image'],$setting['width'], $setting['height'])['image_web_p'];
                $image_old_format = $this->model_tool_image->resizeAndConvertImageWebP($result['image'],$setting['width'],$setting['height'])['image'];

            } else {
                $image = $this->model_tool_image->resizeAndConvertImageWebP('placeholder.png',$setting['width'], $setting['height'])['image_web_p'];
                $image_old_format = $this->model_tool_image->resizeAndConvertImageWebP('placeholder.png',$setting['width'],$setting['height'])['image'];

            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$result['rating'];
            } else {
                $rating = false;
            }

            $products[] = array(
                'product_id'  => $result['product_id'],
                'thumb'       => $image,
                'thumb_old'       => $image_old_format,
                'name'        => $result['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price'       => $price,
                'special'     => $special,
                'tax'         => $tax,
                'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating'      => $result['rating'],
                'href'        => $this->url->link(sprintf($setting['url'], $result['product_id']))
            );
        }


        return $products;
    }
    public function each( $setting ){

        $products = array();
        foreach ($setting['results'] as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resizeAndConvertImageWebP($product_info['image'],'-', '-')['image_web_p'];
                    $image_old_format = $this->model_tool_image->resizeAndConvertImageWebP($product_info['image'],$setting['width'],$setting['height'])['image'];

                } else {
                    $image = $this->model_tool_image->resizeAndConvertImageWebP('placeholder.png',$setting['width'], $setting['height'])['image_web_p'];
                    $image_old_format = $this->model_tool_image->resizeAndConvertImageWebP('placeholder.png',$setting['width'],$setting['height'])['image'];

                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                $products[] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'thumb_old'       => $image_old_format,
                    'name'        => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link(sprintf($setting['url'], $product_info['product_id']))
                );
            }
        }

        return $products;
    }
}
