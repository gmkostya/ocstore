<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-latest" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-latest" class="form-horizontal">
                    <?php if(isset($template_id)) { ?>
                        <input type="hidden" value="<?=$template_id?>" name="template_id">
                    <?php } ?>
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <label for="status">Статус Шаблона</label>
                            <select name="status" id="status" class="form-control">
                                <?php if(isset($template['template']['status']) && $template['template']['status'] == 0){ ?>
                                    <option value="0" selected>Выключить</option>
                                <?php } else { ?>
                                    <option value="0">Выключить</option>
                                <?php } ?>

                                <?php if(isset($template['template']['status']) && $template['template']['status'] == 1){ ?>
                                    <option value="1" selected>Включить</option>
                                <?php } else { ?>
                                    <option value="1">Включить</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <label for="status">Тип Шаблона</label>
                            <select name="type" id="type" class="form-control">
                                <?php if(isset($template['template']['type']) && $template['template']['type'] == 'product'){ ?>
                                    <option value="product" selected>Продукт</option>
                                <?php } else { ?>
                                    <option value="product">Продукт</option>
                                <?php } ?>
                                <?php if(isset($template['template']['type']) && $template['template']['type'] == 'category'){ ?>
                                    <option value="category" selected>Категория</option>
                                <?php } else { ?>
                                    <option value="category">Категория</option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php if(isset($template['template']['type']) && $template['template']['type'] == 'category'){ ?>
                        <div class="col-md-12" id="category_id">
                            <?php } else { ?>
                            <div class="col-md-6 col-sm-6" id="category_id">
                                <?php } ?>
                                <select name="category_id" class="form-control">
                                    <option value="0">--Категория--</option>
                                    <?php foreach($categories as $category) { ?>
                                        <?php if(isset($template['template']['category_id']) && $template['template']['category_id'] == $category['category_id']){ ?>
                                            <option value="<?php echo $category['category_id']; ?>" selected><?php echo $category['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php if(isset($template['template']['type']) && $template['template']['type'] == 'category'){ ?>
                            <div class="col-md-6 col-sm-6" id="manufacturer_id" style="display: none;">
                                <?php } else { ?>
                                <div class="col-md-6 col-sm-6" id="manufacturer_id">
                                    <?php } ?>
                                    <select name="manufacturer_id" class="form-control">
                                        <option value="0">--Бренд--</option>
                                        <?php foreach ($manufacturers as $manufacturer) { ?>
                                            <?php if(isset($template['template']['manufacturer_id']) && $template['template']['manufacturer_id'] == $manufacturer['manufacturer_id']){ ?>
                                                <option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected><?php echo $manufacturer['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div style="margin-top: 10px;">
                                        <label for="name">Назовите Шаблон</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Назовите Шаблон" value="<?php echo isset($template['template']['name']) ? $template['template']['name'] : '';?>">
                                    </div>
                                </div>
                            </div>
                            <ul class="nav nav-tabs" id="language_advert">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language_advert<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language_advert<?php echo $language['language_id']; ?>">
                                        <div class="form-group">
                                            <table id="pr_links_product_<?php echo $language['language_id']; ?>" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <td width="70%" class="text-left">Шаблон</td>
                                                <td width="10%" class="text-right">Привязать</td>
                                                <td width="20%" class="text-right">Описание</td>
                                                </thead>
                                                <tr>
                                                    <td class="text-right">
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][meta_title]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['meta_title']) ? $template['item'][$language['language_id']]['meta_title'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="meta_title">
                                                    </td>
                                                    <?php if(isset($template['template']['type']) && $template['template']['type'] == 'category'){ ?>
                                                    <td rowspan="6" id="rowspan_table">
														<span id="span_description_product" style="display: none">
															<b>Название товара </b>%NAME%</br>
                                                            <b>Производитель </b>%BRAND%</br>
                                                            <b>Главная категория </b>%CATEGORY%</br>
                                                            <b>Модель </b>%MODEL%</br>
                                                            <b>Цена </b>%PRICE% <i>(в зависимости от выбраной валюты на фронте сайта)</i></br>
                                                            <b>Варианты </b>{{дешево|супердешево|дешевле}}</br>
														</span>
                                                        <span id="span_description_category" style="display: block;">
															<b>Главная категория </b>%CATEGORY%</br>
														</span>
                                                        <?php } else { ?>
                                                    <td rowspan="7" id="rowspan_table">
														<span id="span_description_product">
															<b>Название товара </b>%NAME%</br>
                                                            <b>Производитель </b>%BRAND%</br>
                                                            <b>Главная категория </b>%CATEGORY%</br>
                                                            <b>Модель </b>%MODEL%</br>
                                                            <b>Цена </b>%PRICE% <i>(в зависимости от выбраной валюты на фронте сайта)</i></br>
                                                            <b>Варианты </b>{{дешево|супердешево|дешевле}}</br>
														</span>
                                                        <span id="span_description_category" style="display:none;">
															<b>Главная категория </b>%CATEGORY%</br>
														</span>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][meta_desck]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['meta_desck']) ? $template['item'][$language['language_id']]['meta_desck'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="meta_desck">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][h1]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['h1']) ? $template['item'][$language['language_id']]['h1'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="H1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][keywords]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['keywords']) ? $template['item'][$language['language_id']]['keywords'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="keywords">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][name]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['name']) ? $template['item'][$language['language_id']]['name'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="name">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="template[<?php echo $language['language_id']; ?>][description]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['description']) ? $template['item'][$language['language_id']]['description'] : ''?>"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly value="description">
                                                    </td>
                                                </tr>
                                                <?php if(isset($template['template']['type']) && $template['template']['type'] == 'category'){ ?>
                                                    <tr id="tag_tr" style="display: none;">
                                                        <td>
                                                            <input type="text" name="template[<?php echo $language['language_id']; ?>][tag]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['tag']) ? $template['item'][$language['language_id']]['tag'] : ''?>"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" readonly value="tag">
                                                        </td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr id="tag_tr">
                                                        <td>
                                                            <input type="text" name="template[<?php echo $language['language_id']; ?>][tag]" class="form-control" placeholder="Введите свой шаблон" value="<?=isset($template['item'][$language['language_id']]['tag']) ? $template['item'][$language['language_id']]['tag'] : ''?>"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" readonly value="tag">
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 0%;">
                                    0%
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <a class="btn btn-warning center-block" onclick="generate(1)">
                                        <strong>
                                            <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                            Генерация
                                        </strong>
                                    </a>
                                    <div class="alert alert-warning center-block text-center" role="alert" style="margin-top: 20px;">
                                        <strong class="text-danger">
                                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                            Перед генерацией кликните сохранить модуль
                                        </strong>
                                    </div>
                                </div>
                            </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('#language_advert a:first').tab('show');
    //--></script>
<script type="text/javascript">
    $('#type').change(function(){
        var type = $(this).val();
        if(type == 'category'){
            $('#manufacturer_id').slideToggle();
            $('#category_id').removeAttr('class').addClass('col-md-12');
            $('#tag_tr').slideToggle();
            $('#rowspan_table').removeAttr('rowspan').attr('rowspan', '6');
            $('#span_description_product').slideToggle();
            $('#span_description_category').slideToggle();
        } else {
            $('#manufacturer_id').slideToggle();
            $('#category_id').removeAttr('class').addClass('col-md-6 col-sm-6');
            $('#tag_tr').slideToggle();
            $('#rowspan_table').removeAttr('rowspan').attr('rowspan', '7');
            $('#span_description_product').slideToggle();
            $('#span_description_category').slideToggle();
        }
    });
    function updateProgress(percentage){
        if(percentage > 100) percentage = 100;
        $('.progress-bar').attr('aria-valuenow', percentage);
        $('.progress-bar').css('width', percentage+'%');
        $('.progress-bar').html(percentage+'%');
    }

    function generate(){
        updateProgress(0);
        setTimeout(function(){
            $.ajax({
                url: 'index.php?route=module/metatags_create/generate&token=<?php echo $token; ?>',
                dataType: 'json',
                type: 'post',
                data: 'template_id=<?php echo $template_id;?>',
                success: function(json) {

                }
            }).done(function(msg) {
                updateProgress(1*100);
            });
        }, 1000)
    }
</script>

<?php echo $footer; ?>
